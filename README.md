# webextensions-lib-dialog

Provides utility features to provide custom dialog window on WebExtensions based addons for Firefox and Thunderbird.

## Usage on opener side

```javascript
const accepted = await Dialog.open(
  { // parameters for the dialog itself
    url:    'path/to/dialog-example.html',
    modal:  true, // optional, default=false
    opener: null, // optional, windows.Window
    width:  300, // optional, integer
    height: 200, // optional, integer
    left:   150, // optional, integer
    left:   150, // optional, integer
  },
  { // parameters for dialog contents (arbitrary JSON-stringifiable objects)
    message: 'foo',
    count:   30
  }
);
```

## Usage on dialog side

```javascript
window.addEventListener('DOMContentLoaded', async () => {
  // "windowId" is always added, it is the windows.Window.id of this dialog itself.
  const { message, count, windowId } = await Dialog.getParams();

  Dialog.initAcceptButton(document.getElementById('accept'));
  Dialog.initCancelButton(document.getElementById('cancel'));

  mAcceptButton.focus();

  await Dialog.notifyReady();

  // The window can be moved or resized by the Dialog.notifyReady(), so
  // we should start watching of window changes after the process is completed.
  window.addEventListener('resize', () => {
    // you can use this to save the updated size
    console.log('resized: ', {
      width:  window.outerWidth,
      height: window.outerHeight
    });
  });
  window.addEventListener(Dialog.TYPE_MOVED, event => {
    // you can use this to save the updated position
    console.log('moved: ', {
      left: event.detail.left,
      top:  event.detail.top
    });
  });
}, { once: true });
```

