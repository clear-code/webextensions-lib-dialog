/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import * as Dialog from './dialog.js';

const mAcceptButton = document.getElementById('accept');
const mCancelButton = document.getElementById('cancel');

Dialog.initAcceptButton(mAcceptButton);
Dialog.initCancelButton(mCancelButton);

mAcceptButton.focus();
Dialog.notifyReady().then(() => {
  window.addEventListener('resize', () => {
    console.log('resized: ', {
      width:  window.outerWidth,
      height: window.outerHeight
    });
  });
  window.addEventListener(Dialog.TYPE_MOVED, event => {
    console.log('moved: ', {
      left: event.detail.left,
      top:  event.detail.top
    });
  });
});
